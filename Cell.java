import java.awt.*;

public class Cell extends Rectangle {
    Color cellColour= Color.white;
    String grassLength = "" ;

    public Cell(int x, int y) {
        super(x, y, 35, 35);
        randomColour();
    }
    Color myGreen1 =new Color(153,255,153);
    Color myGreen2= new Color(51,255,5);
    Color myGreen3= new Color(0,200,0);
    Color myGreen4= new Color(0,153,0);
    Color myGreen5= new Color(0,105,0);

    public void paint(Graphics g, boolean highlighted) {
        if(highlighted){

            g.setColor(this.cellColour);
            g.fillRect(x, y, 35, 35);
            g.setColor(Color.BLUE);
            g.drawRect(x, y, 35, 35) ;

        }
        else {

            g.setColor(this.cellColour);
            g.fillRect(x, y, 35, 35);
            g.setColor(Color.BLACK);
            g.drawRect(x, y, 35, 35) ;
        }
        //g.setColor(Color.BLACK);
        //g.drawRect(x, y, 35, 35) ;
    }


    @Override
    public boolean contains(Point target){
        if (target == null)
            return false;
        return super.contains(target);
    }
    public void randomColour() {
        java.util.Random ree = new java.util.Random();
        int randomNumber = ree.nextInt(5);
        if (randomNumber == 0) {
            this.cellColour = myGreen1;
            this.grassLength = "Grass length is :1";
        }
        if (randomNumber == 1) {
            this.cellColour = myGreen2;
            this.grassLength = "Grass length is :2";
        }
        if (randomNumber == 2) {
            this.cellColour = myGreen3;
            this.grassLength = "Grass length is :3";
        }
        if (randomNumber == 3) {
            this.cellColour = myGreen4;
            this.grassLength = "Grass length is :4";
        }
        if (randomNumber == 4) {
            this.cellColour = myGreen5;
            this.grassLength = "Grass length is :5";
        }

    }

}